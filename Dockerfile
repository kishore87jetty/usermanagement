FROM openjdk:17

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} UserManagement.jar

ENTRYPOINT ["java","-jar","UserManagement.jar"]

EXPOSE 9090